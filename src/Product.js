import React, { Component } from 'react';
import delImage from './img/del.png';
import detailsImage from './img/details.png';

class Product extends Component {
  constructor(props) {
    super();
    this.state = {
      price: props.price,
      amount: props.amount,
      name: props.name,
      image: props.image,
      id: props.id,
    };
  }

  increase() {
      this.setState({amount: this.state.amount + 1});
      this.props.productListContainer.setProductAmount(this.state.id, this.state.amount+1);
      this.props.productListContainer.refresh();
  }

  decrease() {
      if (this.state.amount > 1) {
          this.setState({amount: this.state.amount - 1});
          this.props.productListContainer.setProductAmount(this.state.id, this.state.amount-1);
          this.props.productListContainer.refresh();
      }
  }

  deleteMe({target}) {
      this.props.productListContainer.deleteProduct(this.state.id);
  }

  showDetails({target}) {
      this.props.productListContainer.showSelectedProduct(this.state.id);
  }

  render() {
    return (
      <div className="product-item">
        <div className="product-header">
             <span>{this.state.name}</span>&nbsp;
             <img src={detailsImage} alt="Details" onClick={this.showDetails.bind(this)} className="right-align" />
             <img src={delImage} alt="Remove" onClick={this.deleteMe.bind(this)} className="right-align" />
        </div>
        <div>
            <div>
                <img src={this.state.image} alt="test" className="left-align" />
                <button id="add-tp-list" name="add-to-list" className="btn-default square" onClick={() => this.decrease()}>
                  -
                </button>
                <span className="square">{this.state.amount}</span>
                <button id="add-tp-list" name="add-to-list" className="btn-default square" onClick={() => this.increase()}>
                  +
                </button>
            </div>
            <div>Total: {this.state.amount * this.state.price} $</div>
        </div>
      </div>
    );
  }
}

export default Product;