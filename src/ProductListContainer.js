import React, { Component } from 'react';
import Product from './Product.js';
import ProductDetails from './ProductDetails.js';

import defaultImg from './img/default.png';


class ProductListContainer extends Component {
  constructor() {
    super();
    var container = this;
    this.state = {
      products: [{amount:3,
                          image:defaultImg,
                          price:3,
                          name:"First",
                          id:0,
                          productListContainer:this},
                  {amount:2,
                          image:defaultImg,
                          price:5,
                          name:"Second",
                          id:1,
                          productListContainer:this}],
      selectedProduct: null,
    };
  }

  addProduct(data) {
        var newId = this.state.products.length;
        var stateCopy = Object.assign({}, this.state);
        stateCopy.products = stateCopy.products.slice();
        var amount = data.amount;
        var image = data.image.src;
        var price = data.price;
        var name = data.name;
        var container = this;
        stateCopy.products.push({amount:amount,
                                         image:image,
                                         price:price,
                                         name:name,
                                         id:newId,
                                         productListContainer:container});
        this.setState(stateCopy);
  }

  deleteProduct(productId) {
      var removedProducts = this.state.products.slice().filter(function(p) {
          return p.id !== productId
      });
      this.setState({products: removedProducts});
  }

  setProductAmount(productId, amount) {
        var stateCopy = Object.assign({}, this.state);
        stateCopy.products = stateCopy.products.slice();
        stateCopy.products[productId] = Object.assign({}, stateCopy.products[productId]);
        stateCopy.products[productId].amount = amount;
        this.setState(stateCopy);
  }

  refresh() {
      this.setState({}); 
  }

  showSelectedProduct(productId) {
      var selectedProduct = this.state.products.filter(function(p) {
          return p.id === productId
      });
      this.setState({selectedProduct: selectedProduct[0]}); 
  }

  showList() {
      this.setState({selectedProduct: null}); 
  }

  renderProductList() {
      if (this.state.selectedProduct != null) {
        return (<span></span>);
      }
      const products = this.state.products.map((p) =>
                                  <Product amount={p.amount}
                                           image={p.image}
                                           price={p.price}
                                           name={p.name}
                                           id={p.id}
                                           productListContainer={p.productListContainer}/>
                       );
      const total = this.renderTotal();
      return (
         <div>
            {products}
            <div className="left-align">Total: {total} $</div>
         </div>
      );
  }

  renderProductDetails() {
      if (this.state.selectedProduct == null) {
        return (<span></span>);
      }
      return (
         <ProductDetails price={this.state.selectedProduct.price}
                     amount={this.state.selectedProduct.amount}
                     name={this.state.selectedProduct.name}
                     image={this.state.selectedProduct.image}
                     productListContainer={this}
                      />
      );
  }
  renderTotal() {
       var total = 0;
       for (var item of this.state.products) {
           total += (item.price*item.amount);
       }

       return (
           <span>
            {total}
           </span>
       );
  }

  render() {
    return (
      <div className="product-list-container">
        <h1>Product List</h1>
        {this.renderProductList()}
        {this.renderProductDetails()}
      </div>
    );
  }
}


export default ProductListContainer;
