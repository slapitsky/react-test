import React, { Component } from 'react';
import Modal from './Modal.js';

import defaultImg from './img/default.png';
import carrotImg from './img/carrot.png';
import appleImg from './img/apple.png';

class ImageSelector extends Component {
    constructor() {
        super();
        this.state = {
          images: [{src:defaultImg,
                    alt:"default",
                    num:0,
                    id:"defaultImg"},
                    {src:carrotImg,
                    alt:"carrot",
                    num:1,
                    id:"carrotImg"},
                    {src:appleImg,
                    alt:"apple",
                    num:2,
                    id:"appleImg"},
                   ],
          selectedImage: {src:defaultImg,
                                             alt:"default",
                                             num:0,
                                             id:"defaultImg"} ,
          isModalOpen: false,
        };

//        this.state.images.push(<img src={defaultImg}
//                                   alt="default"
//                                   onClick={() => this.selectImage(0)}
//                                   id="defaultImg"
//                                   imageSelector={this}/>);
//        this.state.images.push(<img src={carrotImg}
//                                   alt="carrot"
//                                   id="carrotImg"
//                                   onClick={() => this.selectImage(1)}
//                                   imageSelector={this}/>);
//        this.state.images.push(<img src={appleImg}
//                                   alt="apple"
//                                   id="appleImg"
//                                   onClick={() => this.selectImage(2)}
//                                   imageSelector={this}/>);
//
    }

    selectImage(index) {
      this.closeModal();
      var current = this.state.images[index];
      this.setState({ selectedImage: current });
    }

    getSelectedImage() {
        return this.state.selectedImage;
    }

    openModal() {
        this.setState({ isModalOpen: true })
    }

    closeModal() {
        this.setState({ isModalOpen: false })
    }

    renderImageList() {
        const list = this.state.images.map((item) =>
            <span><img src={item.src}
                                           alt={item.alt}
                                           onClick={() => this.selectImage(item.num)}
                                           id={item.id}
                                           imageSelector={this}/>
            </span>
        );
        return (
           <div>
            {list}
           </div>
        );
    }

    render() {
        return (
          <div>
              <div onClick={() => this.openModal()} className="image-selector">
              <img src={this.state.selectedImage.src} alt={this.state.selectedImage.alt} />
              </div>
              <Modal isOpen={this.state.isModalOpen} onClose={() => this.closeModal()}>
                {this.renderImageList()}
              </Modal>
          </div>
        );
    }
}
export default ImageSelector;