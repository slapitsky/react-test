import React, { Component } from 'react';
import ImageSelector from './ImageSelector.js';
import defaultImg from './img/default.png';

class ProductSelector extends Component {
    constructor() {
        super();
        this.state = {
          amount: 1,
          name: "",
          price: 1,
          imageSelector: <ImageSelector ref="ImageSelector"/>,
        };
    }

    handleAdd({target}){
      if (this.state.name!=="" && this.state.price>=1 && this.state.amount>=1) {
          var selected = this.refs.ImageSelector.getSelectedImage();
          var productData = {amount:this.state.amount,
                             name:this.state.name,
                             price:this.state.price,
                             image:selected};

          this.props.handleAddCallback(productData);
          this.reset();
      }
    }

    reset() {
          this.refs.productNameInput.value="";
          this.refs.productPriceInput.value="";
          this.setState({
                    amount: 1,
                    name: "",
                    price: 1,
                    image:<img src={defaultImg} alt="default" id="defaultImg"/>,
                  });
    }

    changeProductName(e) {
      this.setState({ name: e.target.value });
    }

    changeProductPrice(e) {
      this.setState({ price: e.target.value });
    }

    increase() {
        this.setState({amount: this.state.amount + 1});
    }

    decrease() {
        if (this.state.amount > 1) {
            this.setState({amount: this.state.amount - 1});
        }
    }

  render() {
    return (
      <div className="product-selector">
        <h1>Add product to your cart list</h1>
        <div>
          <div>
            <input type="text" className="form-control input-sm product-selector-input" placeholder="Product Name"
            onChange={this.changeProductName.bind(this)} ref="productNameInput"/>
          </div>
          <div>
            <input type="number" min="1" className="form-control input-sm product-selector-input" placeholder="Product Price"
            onChange={this.changeProductPrice.bind(this)} ref="productPriceInput" />
          </div>
          <div>
                <button id="add-tp-list" name="add-to-list" className="btn-default square" onClick={() => this.decrease()}>
                -
                </button>
                <span className="square">{this.state.amount}</span>
                <button id="add-tp-list" name="add-to-list" className="btn-default square" onClick={() => this.increase()}>
                +
                </button>
          </div>
          <div className="image-selector">{this.state.imageSelector}</div>
          <div>
              <button id="add-tp-list" name="add-to-list" value="Add to List" className="btn-default" onClick={this.handleAdd.bind(this)}>
              Add to Cart
              </button>
          </div>
        </div>
      </div>
    );
  }
}
export default ProductSelector;