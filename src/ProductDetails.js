import React, { Component } from 'react';

class ProductDetails extends Component {
  constructor(props) {
    super();
    this.state = {
      price: props.price,
      amount: props.amount,
      name: props.name,
      image: props.image,
      id: props.id,
    };
  }

  backToList() {
      this.props.productListContainer.showList();
  }

  render() {
  var imgPath = this.state.image;
  console.log(imgPath);
    return (
      <div className="product-item-details">
        <h1>{this.state.name}</h1>
        <div><img src={this.state.image} alt="test" /></div>
        <div>Count: {this.state.amount}</div>
        <div>Price: {this.state.price}</div>
        <div>Total: {this.state.price*this.state.amount}</div>
        <button className="btn-default" onClick={() => this.backToList()}>
        Back To List
        </button>
      </div>
    );
  }
}

export default ProductDetails;