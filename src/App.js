import React, { Component } from 'react';
import ProductSelector from './ProductSelector.js';
import ProductListContainer from './ProductListContainer.js';
import './App.css';
import './bootstrap-3.3.7-dist/css/bootstrap.css';

class App extends Component {

   constructor() {
     super();

     this.state = {
          productSelector: <ProductSelector handleAddCallback={this.handleAdd} />,
          productListContainer: <ProductListContainer ref="productListContainer"/>,
     };

  }

  handleAdd = (productData) => {
     this.refs.productListContainer.addProduct(productData);
  }

  render() {
    return (
      <div className="App">
        <div class="main-container">
            {this.state.productSelector}
            {this.state.productListContainer}
            <div className="cleared" />
        </div>
      </div>
    );
  }
}

export default App;
